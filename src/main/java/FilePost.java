/*
---------------------------------------------------------------
     ___      ___ ___ _ ___ ___ 
    |   \ ___/ __/ __(_) __| _ \
    | |) / _ \__ \__ \ | _||   /
    |___/\___/___/___/_|___|_|_\
    
    Database of Scientific Simulated and Experimental Results
---------------------------------------------------------------
 */

import java.io.File;
import java.io.FileInputStream;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

/**
 *
 * @author ncserpico
 */
public class FilePost {

    public static void main(String[] args) throws Exception {

        System.out.println("Sending https POST request");
        
        FilePost post = new FilePost();
        post.sendPost();
    }

    private void sendPost() throws Exception {
        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpPost httpPost = new HttpPost("https://localhost:8181/WebAPI/file/upload");

        //File f = new File("/home/ncserpico/dotti/newdossier.json");
        File dir = new File("/home/ncserpico/julia/json-specs/test19-HARP-json/10.3.p01");
        File[] directoryListing = dir.listFiles();
        for (File f : directoryListing) {
            MultipartEntityBuilder builder = MultipartEntityBuilder.create();
            builder.addBinaryBody(
                    "file",
                    new FileInputStream(f),
                    ContentType.APPLICATION_OCTET_STREAM,
                    f.getName()
            );
            HttpEntity multipart = builder.build();
            httpPost.setEntity(multipart);
            CloseableHttpResponse response = httpClient.execute(httpPost);
            HttpEntity responseEntity = response.getEntity();
            
        }
        
    }
}